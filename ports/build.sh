#!/bin/bash

if [ $# -eq 0 ] ; then
 echo "./build.sh <app>"
 exit
fi

app=$1

. environment
cd $app
make
cp *.ipk ~/script/dev
make clean
cd ..

