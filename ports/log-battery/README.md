### Example result

```json
{
  "VDT": {
    "Battery_StateOfCharge": [
      {
        "value": 254,
        "date": "2018-09-08T21:17:58+0700"
      },
      {
        "value": 184,
        "date": "2018-09-09T22:24:28+0700"
      }
    ]
  }
}
```

## jq command line

```bash
jq .VDT /tmp/root/log-battery/Battery_StateOfCharge.json 
```

