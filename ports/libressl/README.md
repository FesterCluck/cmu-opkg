# Enable Crypto Acceleration

```bash
modprobe cryptodev
```

# Disable Crypto Acceleration

```bash
modprobe -r cryptodev
```

* verify

```bash
ls /dev/crypto
```

## Performance test
The not relate to ''libressl''.  The result was test from original `openssl`.

* Software

```bash
modprobe -r cryptodev
time -v openssl speed -evp aes-128-cbc -elapsed
```

* Hardware Acceleration

```bash
modprobe cryptodev
time -v openssl speed -evp aes-128-cbc -engine cryptodev
```

![performance test](graph.jpg)


## Reference
* [http://trac.gateworks.com/wiki/ventana/encryption](http://trac.gateworks.com/wiki/ventana/encryption)
* [Linux Cryptographic Acceleration on an i.MX6](http://events.linuxfoundation.org/sites/events/files/slides/2017-02%20-%20ELC%20-%20Hudson%20-%20Linux%20Cryptographic%20Acceleration%20on%20an%20MX6.pdf)