PROG=libnvram
VERSION=1.0c
current_dir = $(shell pwd)
TOP=$(current_dir)/..
TOOLCHAIN=$(TOP)/toolchain
FILE=v1.0c.tar.gz
ARM=arm-cortexa9_neon-linux-gnueabi
SOURCE=https://github.com/firmadyne/libnvram/archive/v1.0c.tar.gz
Depends=
Description=This is a library that emulates the behavior of the NVRAM peripheral, by storing key-value pairs into a tmpfs mounted at MOUNT_POINT
Conflicts=
Maintainer=firmadyne
License=MIT License
Section=library

CC=$(TOOLCHAIN)/bin/$(ARM)-gcc
CXX=$(TOOLCHAIN)/bin/$(ARM)-g++
LD=$(TOOLCHAIN)/bin/$(ARM)-ld
AR=$(TOOLCHAIN)/bin/$(ARM)-ar
SYSTEMROOT=$(TOOLCHAIN)/$(ARM)/sysroot

LDFLAGS+=-L$(SYSTEMROOT)/lib -L$(SYSTEMROOT)/usr/lib 
CFLAGS="-Os -s -rdynamic -pthread --sysroot=${SYSTEMROOT} -DCMU=1 -D__STDC_FORMAT_MACROS -march=armv7-a -mtune=cortex-a9 -mfpu=neon"

CMU_INSTALL_PATH=/resources/aio

all: $(PROG)_$(VERSION)_arm.ipk

app: 
ifeq (,$(wildcard $(FILE)))
	wget $(SOURCE)
endif
	tar xf $(FILE) ;\
	cd libnvram-1.0c  ;\
	make ;\
	mkdir -p $(current_dir)/opkg/$(CMU_INSTALL_PATH)/lib ;\
	cp libnvram.so $(current_dir)/opkg/$(CMU_INSTALL_PATH)/lib

$(PROG)_$(VERSION)_arm.ipk: app
	cd $(current_dir)/opkg/ ;\
	sed -i 's/.*Package:.*/Package: $(PROG)/g' CONTROL/control ;\
	sed -i 's/.*Version:.*/Version: $(VERSION)/g' CONTROL/control ;\
	sed -i 's/.*Installed-Size:.*/Installed-Size: $(shell du -sb opkg|cut -f1)/g' CONTROL/control ;\
	sed -i 's,.*Source:.*,Source: $(SOURCE),g' CONTROL/control ;\
	sed -i 's/.*Depends:.*/Depends: $(Depends)/g' CONTROL/control ;\
	sed -i 's/.*Description:.*/Description: $(Description)/g' CONTROL/control ;\
	sed -i 's/.*Conflicts:.*/Conflicts: $(Conflicts)/g' CONTROL/control ;\
	sed -i 's/.*Maintainer:.*/Maintainer: $(Maintainer)/g' CONTROL/control ;\
	sed -i 's/.*License:.*/License: $(License)/g' CONTROL/control ;\
	sed -i 's/.*Section:.*/Section: $(Section)/g' CONTROL/control ;\
	cd $(current_dir) ;\
	opkg-build -o 1018 -g 3015 opkg

sysroot:
ifeq (,$(wildcard $(FILE)))
	wget $(SOURCE)
endif
	tar xf $(FILE) ;\
	cd $(PROG)-$(VERSION); \
	make clean ;\
	./configure --host=$(ARM) ;\
	make install DESTDIR=$(SYSTEMROOT)

clean:
	rm -rf $(PROG)-*
	rm -rf opkg/resources/aio
	rm -f *.ipk
	rm -f *.tar.gz
