#!/bin/sh

action=$1
if [ $# -eq 0 ]; then
 echo "geo.sh <geo|coor|all>"
 exit
fi

GPS=$(/usr/bin/dbus-send --print-reply --address=unix:path=/tmp/dbus_service_socket --type=method_call --dest=com.jci.lds.data /com/jci/lds/data com.jci.lds.data.GetPosition)
GPSSPEED=$(echo "$GPS" | awk 'NR==8{print $2}')
ALTITUDE=$(echo "$GPS" | awk 'NR==6{print $2}')
HEADING=$(echo "$GPS" | awk 'NR==7{print $2}')
LATITUDE=$(echo "$GPS" | awk 'NR==4{print $2}')
LONGITUDE=$(echo "$GPS" | awk 'NR==5{print $2}')
GEOHASH=`geohash  -e ${LATITUDE} ${LONGITUDE}|awk '{print $2}'`

if [ $action == "geo" ];
then
 echo $GEOHASH
elif [ $action == "coor" ]; then
 echo $LATITUDE $LONGITUDE
else
 echo $GEOHASH $LATITUDE $LONGITUDE
fi
