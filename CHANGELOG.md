## [cmu-opkg]

## [dev] - 2017-08-12
- Git version

## [0.3] - 2017-07-22
### Upgrade
- Improve and fix 
 - Videoplayer 2.9
 - Headunit

## [0.2] - 2017-07-05
### Upgrade
- move `/data_persist/dev` to `/resources/aio`

##  [0.1] - 2017-06-27
### Added Package
- mzd-touchscreen 


